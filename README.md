# ImageMagick 7 Lambda Layer

### Build it

```bash
docker run -it --rm -v $PWD/code:/var/task registry.gitlab.com/newman99/image-magick-devel bash
```

### Publish Lambda Layer
```bash
aws lambda publish-layer-version --layer-name ImageMagick --zip-file fileb://code/im.zip --compatible-runtimes python3.6 python3.7 --profile=<aws profile>
```

### Build Docker Image and Push to Registry

```bash
docker login registry.gitlab.com
docker build -t registry.gitlab.com/newman99/image-magick-devel .
docker push registry.gitlab.com/newman99/image-magick-devel
```

### Required libraries

```
libcroco-0.6.so.3
libgdk_pixbuf-2.0.so.0
libgsf-1.so.114
libpango-1.0.so.0
libpangocairo-1.0.so.0
libpangoft2-1.0.so.0
librsvg-2.so.2 
```

### Notes
[Docker lambci/lambda](https://hub.docker.com/r/lambci/lambda/)
[rsvg-convert-aws-lambda-binary](https://github.com/serverlesspub/rsvg-convert-aws-lambda-binary/blob/master/src/compile-static.sh)
[ImageMagick - Installation Instructions](http://www.imagemagick.org/script/install-source.php)
[ImageMagick - Advanced Unix Installation](http://ftp.icm.edu.pl/packages/ImageMagick/www/advanced-unix-installation.html)
[ALAS-2016-699](https://alas.aws.amazon.com/ALAS-2016-699.html)
