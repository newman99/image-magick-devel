#!/bin/bash

# Build it
docker-compose build --no-cache

# Zip it
docker-compose run --rm lambda bash -c "cd /opt && zip --symlinks -r /var/task/im.zip *"

# Publish Lambda Layer
aws lambda publish-layer-version --layer-name ImageMagick --zip-file fileb://code/im.zip --compatible-runtimes python3.6 python3.7 --profile=$AWS_PROFILE

