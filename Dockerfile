FROM lambci/lambda:build-python3.7

ENV PYTHONUNBUFFERED 1

MAINTAINER "Matthew Newman" <newman99@gmail.com>

WORKDIR /var/task

COPY centos6.repo /etc/yum.repos.d

RUN yum clean all \ 
    && yum -y install librsvg2-devel

CMD ["bash"]
